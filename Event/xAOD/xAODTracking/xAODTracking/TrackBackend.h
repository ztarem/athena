/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_TRACKBACKEND_H
#define XAODTRACKING_TRACKBACKEND_H

#include "xAODTracking/versions/TrackBackend_v1.h"

namespace xAOD {
  typedef TrackBackend_v1 TrackBackend;
}
#endif
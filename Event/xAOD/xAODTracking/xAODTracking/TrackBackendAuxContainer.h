/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_TRACKBACKENDAUXCONTAINER_H
#define XAODTRACKING_TRACKBACKENDAUXCONTAINER_H

#include "xAODTracking/versions/TrackBackendAuxContainer_v1.h"

namespace xAOD {
  typedef TrackBackendAuxContainer_v1 TrackBackendAuxContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackBackendAuxContainer , 1264384446 , 1 )
#endif